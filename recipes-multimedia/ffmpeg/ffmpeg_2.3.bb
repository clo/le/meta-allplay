SUMMARY = "A complete, cross-platform solution to record, convert and stream audio and video."
DESCRIPTION = "FFmpeg is the leading multimedia framework, able to decode, encode, transcode, \
               mux, demux, stream, filter and play pretty much anything that humans and machines \
               have created. It supports the most obscure ancient formats up to the cutting edge."
HOMEPAGE = "https://www.ffmpeg.org/"
SECTION = "libs"

PR = "r1"

LICENSE = "LGPLv2.1"

LIC_FILES_CHKSUM = "file://COPYING.LGPLv2.1;md5=bd7a443320af8c812e4c18d1b79df004"

SRC_URI = "\
    https://www.ffmpeg.org/releases/ffmpeg-${PV}.tar.gz \
    file://031-allplay_hls_list.patch \
    file://032-flac_timestamps.patch \
    file://033-http_byte_range_dlna.patch \
    file://049-wav_packet_size.patch \
    file://050-mpegtsenc_remove_extradata_size_check.patch \
    file://064-fixed_read_chunked_stream.patch \
    file://065-flac_parser_fix_nb_headers_buffered.patch \
    file://070-lavf-Set-the-stream-time-base-hint-properly-for-chai.patch \
    "

SRC_URI[md5sum] = "d63e952716c27e23927bfd64518d6dee"
SRC_URI[sha256sum] = "f3b437dbdf9f1519fa5e0a923428e77ba3babefbcfeda9aebb7cd72ae8924c1d"

inherit autotools pkgconfig

DEPENDS = "alsa-lib zlib bzip2 fdk-aac openssl"

do_configure[prefuncs] += "configure_ffmpeg"

ALLPLAY_DECODERS = "libfdk_aac \
    alac \
    dsd_lsbf dsd_lsbf_planar dsd_msbf dsd_msbf_planar \
    flac \
    mp1 \
    mp2 \
    mp3 \
    pcm_alaw \
    pcm_f32be pcm_f32le pcm_f64be pcm_f64le \
    pcm_lxf pcm_mulaw \
    pcm_s16be pcm_s16be_planar pcm_s16le pcm_s16le_planar \
    pcm_s24be pcm_s24daud pcm_s24le pcm_s24le_planar \
    pcm_s32be pcm_s32le pcm_s32le_planar \
    pcm_s8 pcm_s8_planar \
    pcm_u16be pcm_u16le \
    pcm_u24be pcm_u24le \
    pcm_u32be pcm_u32le \
    pcm_u8 \
    vorbis"

ALLPLAY_FORMATS = "aac \
    adts \
    hls,applehttp \
    aiff \
    iff dsf \
    flac \
    mov \
    mp2 \
    mp3 \
    mp4 \
    mpegts \
    ogg \
    wav \
    pcm_alaw pcm_f32be pcm_f32le pcm_f64be pcm_f64le pcm_mulaw \
    pcm_s16be pcm_s16le pcm_s24be pcm_s24le pcm_s32be pcm_s32le pcm_s8 \
    pcm_u16be pcm_u16le pcm_u24be pcm_u24le pcm_u32be pcm_u32le pcm_u8"

ALLPLAY_ENCODERS = "flac"

FFMPEG_CONFIGURE = " \
    --ld="${CCLD}" \
    --cc="${CC}" \
    --cxx="${CXX}" \
    --enable-cross-compile \
    --cross-prefix="${TARGET_PREFIX}" \
    --arch=${TARGET_ARCH} \
    --target-os="linux" \
    --prefix="${prefix}" \
    --sysroot="${STAGING_DIR_TARGET}" \
    --bindir="${bindir}" \
    --datadir="${datadir}/ffmpeg" \
    --libdir="${libdir}" \
    --shlibdir="${libdir}" \
    --incdir="${includedir}" \
    --mandir="${mandir}" \
    --enable-swscale \
    --enable-shared \
    --enable-static \
    --disable-avfilter \
    --disable-debug \
    --pkg-config="pkg-config" \
    --disable-gpl \
    --disable-version3 \
    --disable-nonfree \
    --disable-asm \
    --disable-mipsfpu \
    --disable-doc \
    --disable-dxva2 \
    --disable-outdev="sdl" \
    --enable-pthreads \
    --enable-small \
    --disable-stripping \
    --enable-zlib \
    --enable-hardcoded-tables \
    --enable-libfdk-aac \
    --disable-everything \
    --enable-indevs \
    --enable-outdevs \
    --enable-muxer="spdif" \
    --enable-openssl \
    --enable-protocols \
    --extra-cflags="${TARGET_CFLAGS} ${HOST_CC_ARCH}${TOOLCHAIN_OPTIONS}" \
    --extra-ldflags="${TARGET_LDFLAGS}" \
"

CONFIG_DECODERS ?= ""
CONFIG_ENCODER ?= ""
CONFIG_MUXER ?= ""
CONFIG_DEMUXER ?= ""

python configure_ffmpeg() {
    variants = d.getVar("ALLPLAY_DECODERS", True)
    for item in variants.split():
        confoption = '--enable-decoder='+item
        d.appendVar("CONFIG_DECODERS", " " + confoption)

    variants = d.getVar("ALLPLAY_ENCODERS", True)
    for item in variants.split():
        confoption = '--enable-encoder='+item
        d.appendVar("CONFIG_ENCODERS", " " + confoption)

    variants = d.getVar("ALLPLAY_FORMATS", True)
    for item in variants.split():
        confoption_muxer = '--enable-muxer='+item
        confoption_demux = '--enable-demuxer='+item
        d.appendVar("CONFIG_MUXER", " " + confoption_muxer)
        d.appendVar("CONFIG_DEMUXER", " " + confoption_demux)

    d.appendVar("FFMPEG_CONFIGURE", " " +  d.getVar('CONFIG_ENCODERS', True))
    d.appendVar("FFMPEG_CONFIGURE", " " +  d.getVar('CONFIG_DECODERS', True))
    d.appendVar("FFMPEG_CONFIGURE", " " +  d.getVar('CONFIG_MUXER', True))
    d.appendVar("FFMPEG_CONFIGURE", " " +  d.getVar('CONFIG_DEMUXER', True))
    bb.note(d.getVar('FFMPEG_CONFIGURE', True))
}

CFLAGS += "-fPIC"
CPPFLAGS += "-fPIC"

do_configure() {
    ${S}/configure ${FFMPEG_CONFIGURE}
}

PACKAGES_DYNAMIC += "^lib(av(codec|device|format|util|resample)|swscale|swresample|postproc).*"
